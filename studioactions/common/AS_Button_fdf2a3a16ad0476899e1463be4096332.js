function AS_Button_fdf2a3a16ad0476899e1463be4096332(eventobject) {
    var self = this;
    self.view.imgPkg.src = "package_normal.png";
    self.view.imgOficinas.src = "office_selected.png";
    self.view.imgUsuario.src = "option3.png";
    if (kony.theme.getCurrentTheme() != "default") {
        kony.theme.setCurrentTheme("default", function() {
            self.view.lblOficinas.skin = "sknlblFocus";
        }, null);
    } else {
        (function() {
            self.view.lblOficinas.skin = "sknlblFocus";
        })();
    }
    if (kony.theme.getCurrentTheme() != "default") {
        kony.theme.setCurrentTheme("default", function() {
            self.view.lblPkg.skin = "sknlblNormal";
        }, null);
    } else {
        (function() {
            self.view.lblPkg.skin = "sknlblNormal";
        })();
    }
    if (kony.theme.getCurrentTheme() != "default") {
        kony.theme.setCurrentTheme("default", function() {
            self.view.lblUsuario.skin = "sknlblNormal";
        }, null);
    } else {
        (function() {
            self.view.lblUsuario.skin = "sknlblNormal";
        })();
    }
}