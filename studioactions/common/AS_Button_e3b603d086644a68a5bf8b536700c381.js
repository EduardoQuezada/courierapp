function AS_Button_e3b603d086644a68a5bf8b536700c381(eventobject) {
    var self = this;
    self.view.imgPkg.src = "package_normal.png";
    self.view.imgOficinas.src = "office_normal.png";
    self.view.imgUsuario.src = "contacts_active.png";
    if (kony.theme.getCurrentTheme() != "default") {
        kony.theme.setCurrentTheme("default", function() {
            self.view.lblPkg.skin = "sknlblNormal";
        }, null);
    } else {
        (function() {
            self.view.lblPkg.skin = "sknlblNormal";
        })();
    }
    if (kony.theme.getCurrentTheme() != "default") {
        kony.theme.setCurrentTheme("default", function() {
            self.view.lblOficinas.skin = "sknlblNormal";
        }, null);
    } else {
        (function() {
            self.view.lblOficinas.skin = "sknlblNormal";
        })();
    }
    if (kony.theme.getCurrentTheme() != "default") {
        kony.theme.setCurrentTheme("default", function() {
            self.view.lblUsuario.skin = "sknlblFocus";
        }, null);
    } else {
        (function() {
            self.view.lblUsuario.skin = "sknlblFocus";
        })();
    }
}