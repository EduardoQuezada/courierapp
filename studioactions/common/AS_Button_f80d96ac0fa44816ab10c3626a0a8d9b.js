function AS_Button_f80d96ac0fa44816ab10c3626a0a8d9b(eventobject) {
    var self = this;
    if (kony.theme.getCurrentTheme() != "default") {
        kony.theme.setCurrentTheme("default", function() {
            self.view.lblOficinas.skin = "sknlblNormal";
        }, null);
    } else {
        (function() {
            self.view.lblOficinas.skin = "sknlblNormal";
        })();
    }
    if (kony.theme.getCurrentTheme() != "default") {
        kony.theme.setCurrentTheme("default", function() {
            self.view.lblUsuario.skin = "sknlblNormal";
        }, null);
    } else {
        (function() {
            self.view.lblUsuario.skin = "sknlblNormal";
        })();
    }
    if (kony.theme.getCurrentTheme() != "default") {
        kony.theme.setCurrentTheme("default", function() {
            self.view.lblPkg.skin = "sknlblFocus";
        }, null);
    } else {
        (function() {
            self.view.lblPkg.skin = "sknlblFocus";
        })();
    }
    self.view.imgPkg.src = "package_selected.png";
    self.view.imgOficinas.src = "office_normal.png";
    self.view.imgUsuario.src = "option3.png";
}