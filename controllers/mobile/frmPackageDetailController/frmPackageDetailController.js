serviceName = "CourierAppEdMgnt";
integrationObj = KNYMobileFabric.getIntegrationService(serviceName);


define({ 

  onNavigate:function(row){

    this.view.lblDetailTitle.text = row.title;

    this.view.lblWeight.text = "Peso: "+row.weight+" Libra(s)"; 
    this.view.lblPrice.text ="Precio retirar: DOP "+row.priceToPay;
    this.view.lblInternalTracking.text =  "Tracking interno: "+row.internalTracking;
    this.view.lblSupplier.text = "Suplidor: "+row.supplier;
    this.view.lblCourier.text ="Transportista: "+row.courier;
    this.view.lblCourierTracking.text = "Tracking transportista: "+row.courierTracking;

kony.application.showLoadingScreen();
 this.pauseNavigation();   
    operationName =  "getStatus";
    data= {"internalTracking": row.internalTracking};
    headers= {};
    integrationObj.invokeOperation(operationName, headers, data, this.operationSuccess, this.operationFailure);


  },

  operationSuccess:function(res){

    packageTrackingData = res.responseObject.reverse();
    var packageTracking=[];
    var pkgTrackingDetail={};
    
    this.view.segPkgTracking.widgetDataMap = {


      trackingStatus:'description',
      trackingDate:'date',
      trackingImg:'image'
	

    };

     for (var i=0 ; i < packageTrackingData.length; i++){
             
       pkgTrackingDetail={};
       
       pkgTrackingDetail.description=packageTrackingData[i].description;
        
      	pkgTrackingDetail.date=packageTrackingData[i].date;
      
 
       if (pkgTrackingDetail.description==='Recibido miami'){pkgTrackingDetail.image='box.png';}
      if (pkgTrackingDetail.description==='Disponible para retirar'){pkgTrackingDetail.image='green_check.png';}
       if (pkgTrackingDetail.description==='Retenido por aduanas'){pkgTrackingDetail.image='icono_warning_bps.png';}
       if (pkgTrackingDetail.description==='Recibido AILA'){pkgTrackingDetail.image='almacen.png';}
       if (pkgTrackingDetail.description==='En almacen'){pkgTrackingDetail.image='almacen.png';}
       if (pkgTrackingDetail.description==='Embarcado'){pkgTrackingDetail.image='avion.png';}

       
      packageTracking.push(pkgTrackingDetail);


      this.view.segPkgTracking.setData(packageTracking);
       
     }
    kony.application.dismissLoadingScreen(); 
    this.resumeNavigation();
    
//     this.view.segPkgTracking.setData(packageTrackingData.reverse());


  },

  operationFailure:function(res){
    alert("Could not get tracking Info.");
  }


});