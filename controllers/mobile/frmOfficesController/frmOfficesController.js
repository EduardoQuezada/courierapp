	serviceName = "CourierAppEdMgnt";
	integrationObj = KNYMobileFabric.getIntegrationService(serviceName);



define({ 

  
  onNavigate:function(){
    
    kony.application.showLoadingScreen();
    this.pauseNavigation();
    operationName =  "Branches";
data= {};
headers= {};
integrationObj.invokeOperation(operationName, headers, data, this.operationSuccess, this.operationFailure);
    
     
    this.setSkin();

   	this.view.MainHeader.btnUsuario.onClick = this.gotoPreferences;
    this.view.MainHeader.btnPkg.onClick = this.gotoPackages;
  },
    gotoPackages:function(){
    var ntf = new kony.mvc.Navigation("frmPackages");
    ntf.navigate();
    
  },
  

    gotoPreferences:function(){
    
    var ntf = new kony.mvc.Navigation("frmPreferences");
    ntf.navigate();
  },
    setSkin:function(){
    
    this.view.MainHeader.lblPkg.skin = "sknlblNormal";
    this.view.MainHeader.imgPkg.src= "package_normal.png";
        this.view.MainHeader.lblOficinas.skin = "sknlblFocus";
    this.view.MainHeader.imgOficinas.src= "office_selected.png";
        this.view.MainHeader.lblUsuario.skin = "sknlblNormal";
    this.view.MainHeader.imgUsuario.src= "option3.png";
  },
  
  operationSuccess:function(res){
    
  
      //code for success call back
    officeData = res.responseObject;
	var officeDataInfo=[];
    var officeDataDetail={};
    var index = 1;
    
  
    
    this.view.segOfficesData.widgetDataMap = {
     
      lblTitle: 'name',
     lblNumber:'index'
    
  };
       
       
       
  for (var i=0; i<officeData.length;i++){
    
	officeDataDetail={};
    officeDataDetail.name=officeData[i].name;
    officeDataDetail.phoneNumber=officeData[i].phoneNumber;
    officeDataDetail.address=officeData[i].address;
    officeDataDetail.workingHours=officeData[i].workingHours;
    officeDataDetail.email=officeData[i].email;
    officeDataDetail.latitude=officeData[i].latitude;
    officeDataDetail.longitude=officeData[i].longitude;
    officeDataDetail.index=index.toString();
        
    index++;
    
    officeDataInfo.push(officeDataDetail);
    
    this.view.segOfficesData.setData(officeDataInfo);
  }
    
   kony.application.dismissLoadingScreen(); 
    this.resumeNavigation();
  },
  
    operationFailure:function(res){
alert("Could not run Service.");
  }
  
  
  
 });