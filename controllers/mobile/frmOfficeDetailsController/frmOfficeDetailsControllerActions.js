define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for headerButtonLeft **/
    AS_Button_d34d2730925742aca5be9fd21713bdc2: function AS_Button_d34d2730925742aca5be9fd21713bdc2(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmOffices");
        ntf.navigate();
    },
    /** onClick defined for flxPhone **/
    AS_FlexContainer_e531e4145bec4ca1acc4d8d6d338955a: function AS_FlexContainer_e531e4145bec4ca1acc4d8d6d338955a(eventobject) {
        var self = this;

        function SHOW_ALERT__i65cfce0997748b2a6335b5cca9a7730_True() {
            function CallTheNumber() {
                try {
                    kony.phone.dial(row.phoneNumber);
                } catch (err) {
                    alert("error in dial:: " + err);
                }
            }
        }
        function SHOW_ALERT__i65cfce0997748b2a6335b5cca9a7730_False() {}
        function SHOW_ALERT__i65cfce0997748b2a6335b5cca9a7730_Callback(response) {
            if (response === true) {
                SHOW_ALERT__i65cfce0997748b2a6335b5cca9a7730_True();
            } else {
                SHOW_ALERT__i65cfce0997748b2a6335b5cca9a7730_False();
            }
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_CONFIRMATION,
            "alertTitle": "Llamar a la Oficina",
            "yesLabel": "Llamar",
            "noLabel": "No",
            "alertIcon": "icons8_phone_40.png",
            "message": "Desea llamar a esta oficina?",
            "alertHandler": SHOW_ALERT__i65cfce0997748b2a6335b5cca9a7730_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_LEFT
        });
    }
});