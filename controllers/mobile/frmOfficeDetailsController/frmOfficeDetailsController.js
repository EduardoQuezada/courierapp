define({ 

  
  
onNavigate:function(row){
  

  this.view.lblOfficeTitle.text = 'Oficina '+row.name;
	this.view.lblPhoneNumber.text = row.phoneNumber;
  this.view.lblAddressData.text = row.address;
  this.view.lblScheduleData.text = row.workingHours;
  this.view.lblEmailData.text = row.email;
   var locationData = {
    lat: row.latitude,
    lon: row.longitude,
    name: "Mapa",
    desc: "Mapa Local"
};
this.view.mapOfficeDetails.navigateToLocation(locationData, true, true);
  
 this.view.mapOfficeDetails.zoomLevel=25;
  
}
 
  


 });