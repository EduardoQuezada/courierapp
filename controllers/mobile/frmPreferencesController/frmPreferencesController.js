define({ 

 onNavigate:function(){
   
   
   //Load Account Information
   this.view.lblUserAccount.text=userInfo.accountNumber;
   this.view.lblUserName.text=userInfo.fullName;
   
   //Set Navigation
    this.view.MainHeader.btnOficinas.onClick = this.gotoOffices;
    this.view.MainHeader.btnPkg.onClick = this.gotoPackages;
   this.view.btnClose.onClick=this.closeSession;
   
   //Set Timeout Function
	this.timeOut();
   
   //Apply Styles
   this.setSkin();
   
 },
  timeOut: function(){
  
   kony.application.registerForIdleTimeout(1, this.closeSession);
  },
  
  closeSession:function(){
    var ntf = new kony.mvc.Navigation("frmLogin");
    ClearLogin=true;
    ntf.navigate();
    
  },
  
    gotoPackages:function(){
    var ntf = new kony.mvc.Navigation("frmPackages");
    ntf.navigate();
    
  },
  
 
  gotoOffices:function(){
    
    var ntf = new kony.mvc.Navigation("frmOffices");
    ntf.navigate();
  },
  
    setSkin:function(){
    
    this.view.MainHeader.lblPkg.skin = "sknlblNormal";
    this.view.MainHeader.imgPkg.src= "package_normal.png";
        this.view.MainHeader.lblOficinas.skin = "sknlblNormal";
    this.view.MainHeader.imgOficinas.src= "office_normal.png";
        this.view.MainHeader.lblUsuario.skin = "sknlblFocus";
    this.view.MainHeader.imgUsuario.src= "contacts_active.png";
  }

  
 });