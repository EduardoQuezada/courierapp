serviceName = "CourierAppEdMgnt";
integrationObj = KNYMobileFabric.getIntegrationService(serviceName);

define({ 


  onNavigate: function(user){
    
 
    
    //Get Packages from the User
    
operationName =  "Packages";
data= {"username": user};
headers= {};
integrationObj.invokeOperation(operationName, headers, data, this.operationSuccess, this.operationFailure);
    
   kony.application.showLoadingScreen();
    this.pauseNavigation();
	//Skin Buttons according to current Form
    
   	this.setSkin();
    this.view.MainHeader.btnOficinas.onClick = this.gotoOffices;
   	this.view.MainHeader.btnUsuario.onClick = this.gotoPreferences;
    this.view.MainHeader.btnPkg.onClick = this.gotoPackages;
  },
  
  operationSuccess:function(res){
    

    
      //get Package Data
    packageData = res.responseObject;
    packages=[];
	var pkgDetail={};
    
    
    this.view.segPkgData.widgetDataMap = {

      pkgTitle: 'title',
      pkgDesc:'desc',
      pkgDate:'date',
      pkgImg:'image'
 
  };
    for (var i=0 ; i < packageData.length; i++){
      var length = packageData[i].statusHistory.length - 1;
      var status = packageData[i].statusHistory[length] ;
      var datePkg = status.date;
      var descriptionPkg = status.description;
	
      pkgDetail={};

      pkgDetail.date=datePkg;
      pkgDetail.desc=descriptionPkg;
      pkgDetail.title=packageData[i].description;
      pkgDetail.internalTracking=packageData[i].internalTracking;
      pkgDetail.courierTracking=packageData[i].courierTracking;
      pkgDetail.courier=packageData[i].courier;
      pkgDetail.supplier=packageData[i].supplier;
      pkgDetail.priceToPay=packageData[i].priceToPay;
      pkgDetail.weight=packageData[i].weight;
     
      if(descriptionPkg==='Recibido miami'){
       pkgDetail.image='box.png';
      }
       if(descriptionPkg==='Disponible para retirar'){
       pkgDetail.image='green_check.png';
      }
       if(descriptionPkg==='Retenido por aduanas'){
       pkgDetail.image='icono_warning_bps.png';
      }
       if(descriptionPkg==='En Almacén'){
       pkgDetail.image='almacen.png';
      }
         if(descriptionPkg==='Embarcado'){
       pkgDetail.image='avion.png';
      }
   
      
      packages.push(pkgDetail);


      this.view.segPkgData.setData(packages);
   


}
  kony.application.dismissLoadingScreen();
  this.resumeNavigation();  
    
  },

   operationFailure:function(res){
alert("Could not Run Service");
  },
  
  
 
  gotoOffices:function(){
    
    var ntf = new kony.mvc.Navigation("frmOffices");
    ntf.navigate();
  },
    gotoPreferences:function(){
    
    var ntf = new kony.mvc.Navigation("frmPreferences");
    ntf.navigate();
  },
  
  setSkin:function(){
    
    this.view.MainHeader.lblPkg.skin = "sknlblFocus";
    this.view.MainHeader.imgPkg.src= "package_selected.png";
        this.view.MainHeader.lblOficinas.skin = "sknlblNormal";
    this.view.MainHeader.imgOficinas.src= "office_normal.png";
        this.view.MainHeader.lblUsuario.skin = "sknlblNormal";
    this.view.MainHeader.imgUsuario.src= "option3.png";
  },
  
  



 });